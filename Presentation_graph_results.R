
library(ggplot2)
library(dplyr)

data <- read.csv("~/smlsamplingeu/résultat_lulucf.csv", sep = ";")
# data <- read.csv("~/smlsamplingeu/résultats_clc.csv", sep = ";")

# Reclasser les modalités de la colonne rmqs
data$rmqs <- factor(data$rmqs, levels = c("sans", "tout", "2425"))

ggplot(data, aes(x = rmqs, y = nb_sites, fill = rmqs)) +
  geom_bar(stat = "identity", position = "dodge", color = "black", width = 0.67) +  # Réduction de la largeur des barres
  geom_text(
    aes(label = nb_sites),                       # Ajout des étiquettes
    position = position_dodge(width = 0.67),    # Alignement des étiquettes avec les barres
    vjust = -0.5,                               # Positionnement au-dessus des barres
    fontface = "bold",                          # Texte en gras
    color = "black",                            # Texte noir
    size = 3.5                                  # Taille légèrement réduite
  ) +
  geom_hline(aes(yintercept = 2240, linetype = "Nombre de sites RMQS"), 
             color = "black", size = 0.8) +     # Ligne horizontale avec légende
  scale_linetype_manual(values = c("Nombre de sites RMQS" = "dashed")) +  # Type de ligne associé à la légende
  facet_grid(resolution ~ ensemble + sol, scales = "free") +              # Facettage par résolution, ensemble et sol
  labs(
    title = "Répartition des sites selon la prise en compte du RMQS (XXX)",
    x = "Prise en compte du RMQS",
    y = "Nombre de sites",
    fill = "rmqs",
    linetype = NULL  # Titre pour la légende de la ligne
  ) +
  theme_minimal() +
  theme(
    strip.text = element_text(size = 10),        # Taille du texte dans les facettes
    axis.text.x = element_text(angle = 45,      # Rotation des noms d'axes
                               hjust = 1),
    plot.title = element_text(hjust = 0.5,      # Centrage du titre
                              size = 14),
    panel.spacing = unit(0.5, "lines"),         # Espacement entre les facettes
    panel.border = element_rect(color = "black", # Bordures noires autour des facettes
                                fill = NA, 
                                size = 1)
  )



# ggplot(data, aes(x = rmqs, y = nb_supp, fill = rmqs)) +
#   geom_bar(stat = "identity", position = "dodge", color = "black") +  # Barres colorées avec contours
#   facet_grid(resolution ~ ensemble + sol, scales = "free") +      # Facettage par résolution, scénario et source
#   labs(
#     title = "Répartition des sites supplémentaires au RMQS",
#     x = "Prise en compte du RMQS",
#     y = "Nombre de sites supplémentaires",
#     fill = ""
#   ) +
#   theme_minimal() +
#   theme(
#     strip.text = element_text(size = 10),     # Taille du texte dans les facettes
#     axis.text.x = element_text(angle = 45,   # Rotation des noms d'axes
#                                hjust = 1),
#     plot.title = element_text(hjust = 0.5,   # Centrage du titre
#                               size = 14)
#   ) +
#   geom_text(aes(label = nb_supp),           # Affichage du nombre de sites au sommet des barres
#             position = position_dodge(width = 0.8), vjust = -0.2, fontface = "bold", size = 3.5, color = "black") +
#  
#   scale_linetype_manual(name = "Légende", values = "dashed", labels = "Nombre de sites RMQS") + # Légende pour la ligne
#   theme(legend.position = "top", 
#         panel.border = element_rect(color = "black", # Bordures noires autour des facettes
#                                     fill = NA, 
#                                     size = 1))

library(ggplot2)
library(dplyr)

# Filtrer les données pour exclure la modalité "sans"
data_filtered <- data %>%
  filter(rmqs != "sans") %>% 
  mutate(rmqs = factor(rmqs, levels = c("tout", "2425")))

# Créer le graphique avec les données filtrées
ggplot(data_filtered, aes(x = rmqs, y = nb_supp, fill = rmqs)) +
  geom_bar(stat = "identity", position = "dodge", color = "black") +  # Barres colorées avec contours
  facet_grid(resolution ~ ensemble + sol, scales = "free") +      # Facettage par résolution, scénario et source
  labs(
    title = "Répartition des sites supplémentaires au RMQS (LULUCF)",
    x = "Prise en compte du RMQS",
    y = "Nombre de sites supplémentaires",
    fill = ""
  ) +
  theme_minimal() +
  theme(
    strip.text = element_text(size = 10),     # Taille du texte dans les facettes
    axis.text.x = element_text(angle = 45,   # Rotation des noms d'axes
                               hjust = 1),
    plot.title = element_text(hjust = 0.5,   # Centrage du titre
                              size = 14)
  ) +
  geom_text(aes(label = nb_supp),           # Affichage du nombre de sites au sommet des barres
            position = position_dodge(width = 0.8), vjust = -0.2, fontface = "bold", size = 3.5, color = "black") +
  scale_linetype_manual(name = "Légende", values = "dashed", labels = "Nombre de sites RMQS") + # Légende pour la ligne
  scale_fill_manual(values = c("2425" = "#649cfc", "tout" = "#16ba3f")) +  # Couleurs personnalisées
  theme(legend.position = "top", 
        panel.border = element_rect(color = "black", # Bordures noires autour des facettes
                                    fill = NA, 
                                    size = 1))













# ############

# Chargement des bibliothèques
library(terra)
library(ggplot2)
library(dplyr)

# 1. Vérifier ou reprojeter le raster si nécessaire
# Si votre raster n'est pas encore en EPSG:3035, reprojetez-le :
# domains <- project(domains, "EPSG:3035")

# 2. Convertir le raster "domains" en dataframe
domains_df <- as.data.frame(domains, xy = TRUE, na.rm = TRUE)

# 3. Définir les limites pour le zoom sur la région Centre-Val de Loire (EPSG:3035)
zoom_xmin <- 620000
zoom_xmax <- 750000
zoom_ymin <- 5650000
zoom_ymax <- 5770000

# 4. Filtrer les données pour inclure uniquement les points dans la zone de Centre-Val de Loire
domains_zoom_df <- domains_df %>%
  filter(x >= zoom_xmin & x <= zoom_xmax & y >= zoom_ymin & y <= zoom_ymax)

# 5. Ajouter les coordonnées d'Orléans (x, y en EPSG:3035)
orleans_coords <- data.frame(x = 685000, y = 5735000)

# 6. Créer la carte avec ggplot, en ajoutant le point d'Orléans
ggplot(domains_zoom_df, aes(x = x, y = y, fill = FR_landuse)) + # Remplacez "layer" par le vrai nom de la colonne si nécessaire
  geom_tile() +
  scale_fill_viridis_c(name = "Domains") + # Palette de couleurs
  coord_fixed() +
  geom_point(data = orleans_coords, aes(x = x, y = y), color = "red", size = 3, shape = 21, fill = "white") +  # Ajouter le point d'Orléans
  labs(
    title = "Zoom sur la région Centre-Val de Loire avec Orléans (France) - EPSG:3035",
    x = "Longitude (EPSG:3035)",
    y = "Latitude (EPSG:3035)"
  ) +
  theme_minimal()

# 7. Sauvegarder la carte
ggsave("zoom_centre_val_de_loire_orleans_point.png", dpi = 300, width = 10, height = 7)
