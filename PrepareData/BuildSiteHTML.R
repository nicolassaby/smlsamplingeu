library(rmarkdown)
library(prettydoc)
library(rmdformats)

rmarkdown::render("PrepareData/plotResults.Rmd", 
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")),
                  params = list(MySite = site ),
                  output_dir = 'public/',
                  output_file = 'index.html',
                  # output_format = c("html_document"),
                  encoding="UTF-8"
)


rmarkdown::render("PrepareData/plotResultsFillTable.Rmd", 
                  readthedown(highlight = "kate", 
                              gallery = TRUE,
                              fig_width = 12,
                              fig_height = 12,
                              # toc = TRUE,
                              # toc_float = TRUE,
                              code_folding = c("hide")),
                  params = list(MySite = site ),
                  output_dir = 'public/',
                  output_file = 'index.html',
                  # output_format = c("html_document"),
                  encoding="UTF-8"
)


