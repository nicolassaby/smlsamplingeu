library(sf)
library(terra)
library(ggplot2)
library(dplyr)
library(tidyr) 
library(SamplingStrata) 
library(clhs) 
library(viridis)
library(tmap)

# 1 Data preparation -----

setwd("~/serena/data/sml/europe/")
stacka <- rast(c("FR_OC.tif", "FR_pH.tif" , "FR_TXT.tif","FR_N.tif","FR_P.tif",
                 "FR_CEC.tif","FR_BD010.tif") ) 

stackb <- rast(c("FR_CLC.tif",  "FR_NUTS2.tif","FR_NUTS1.tif")) 
stack = c(stacka,stackb)

change = FALSE # set true to resample rasters

if(change) {
  # Change resolution
  stac500a <- terra::aggregate(stacka, 5, fun=mean)
  stac500b <- terra::aggregate(stackb, 5, fun=modal)
  stack500 = c(stac500a,stac500b)
  
  terra::writeRaster(stack500,filename = "~/serena/data/sml/europe/stack500.tiff",
                     overwrite = TRUE)
  
} else {
  stack500 = rast("~/serena/data/sml/europe/stack500.tiff")
  
}


setwd("~/serena/smlsamplingeu")


# JRC configuration -----------

set.seed(123) # not done by JRC but important 


# prepare 500 m starting sample

sample1 <- spatSample(stack,
                      80000,
                      na.rm=TRUE ,
                      "random", 
                      values=TRUE, 
                      xy=TRUE)

sample1 <- filter(sample1, !FR_CLC %in% c(1:9, 30,31, 33, 34, 35, 37:43))


sample1$FR_CLC <- as.factor(sample1$FR_CLC)
sample1$FR_NUTS2 <- as.factor(sample1$FR_NUTS2)
sample1$FR_NUTS1 <- as.factor(sample1$FR_NUTS1)

# prepare 500 m starting sample
sample1500 <- spatSample(stack500,
                      80000,
                      na.rm=TRUE ,
                      "random", 
                      values=TRUE, 
                      xy=TRUE)

sample1500 <- filter(sample1500, !FR_CLC %in% c(1:9, 30,31, 33, 34, 35, 37:43))


sample1500$FR_CLC <- as.factor(sample1500$FR_CLC)
sample1500$FR_NUTS2 <- as.factor(sample1500$FR_NUTS2)
sample1500$FR_NUTS1 <- as.factor(sample1500$FR_NUTS1)




#Replace default kmeans function to use a different algorithm (to avoid convergence issues)
kmeans.default <- function(x, algorithm="MacQueen", ...){base::kmeans.default(x, algorithm, ...)}

source("functionssML.R")

# 1 Test A0 100 m-----

# Choice of the raster to use in the optimisation process
X = Y = c("FR_OC",  "FR_pH")


rnd.lhs = clhs::clhs(sample1[ , X ],
                     size=5000,
                     iter=20000,
                     progress=T,
                     simple=F
                     
)

sample2 <- sample1[rnd.lhs$index_samples,]

sample2$id <- c(1:nrow(sample2))

ndom <- length(unique(sample2$FR_NUTS1))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))



resNUTS1 = optimisesampleSML(
  framesamp = sample2 ,
  framecens= NULL ,
  X = X ,
  Y = Y ,
  domainvalue = "FR_NUTS1" ,
  cv = cv
)


plotResu(resNUTS1,
         nom = "NUTS1pHC",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
         )




# 2 Test A1 -----
## 2.1 all properties NUTS1 --------

X= Y = c("FR_OC",  "FR_pH","FR_TXT_1",  "FR_TXT_2",  "FR_P","FR_N","FR_BD010")

rnd.lhs = clhs::clhs(sample1[ , X ],
                     size=5000,
                     iter=20000,
                     progress=T,
                     simple=F
                     
)

sample2 <- sample1[rnd.lhs$index_samples,]
sample2$id <- c(1:nrow(sample2))
ndom <- length(unique(sample2$FR_NUTS1))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


resNUTS1Sallprop = optimisesampleSML(
  framesamp = sample2 ,
  framecens= NULL ,
  X = X ,
  Y = Y ,
  domainvalue = "FR_NUTS1" ,
  cv = cv
)



plotResu(resNUTS1Sallprop,
         nom = "NUTS1all",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)


## 2.2 Test A1 with a larger sample -----

sample2 <- sample1


sample2$id <- c(1:nrow(sample2))


X= Y = c("FR_OC",  "FR_pH","FR_TXT_1",  "FR_TXT_2",  "FR_P","FR_N","FR_BD010")


ndom <- length(unique(sample2$FR_NUTS1))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

resNUTS1allpropLargeSample = optimisesampleSML(
  framesamp = sample2 ,
  framecens= NULL ,
  X = X ,
  Y = Y ,
  domainvalue = "FR_NUTS1" ,
  cv = cv
)



plotResu(resNUTS1allpropLargeSample,
         nom = "NUTS1allLarge",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)



## 2.3 With a grid as started sample --------------

sampleG <- spatSample(stack,
                      50000,
                      na.rm=TRUE ,
                      "regular", 
                      values=TRUE, 
                      xy=TRUE)

sample2 <- filter(sampleG, !FR_CLC %in% c(1:9, 29, 30,31, 33, 34, 35, 37:43))
sample2$FR_CLC <- as.factor(sample2$FR_CLC)
sample2$FR_NUTS2 <- as.factor(sample2$FR_NUTS2)
sample2$FR_NUTS1 <- as.factor(sample2$FR_NUTS1)

sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)


sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS1))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


resGRid10k = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

plotResu(resGRid10k,
         nom = "Nuts1AllGRid10k",
         crs = crs(stack500),
         sample2 = sample3,
         stack500 = stack500
)



# 3 Test with LU x NUTS1 -------------

## 3.1 with 100 m and clhs subsample -------------


sample2 <- sample1[rnd.lhs$index_samples,]

# simplified LU
sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)

sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS1))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 

sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample2$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

resCLCNUTS1clhs = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)



plotResu(resCLCNUTS1clhs,
         nom = "NUTS1allclhs",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)






## 3.1 with 100 m and full data -------------

sample2 <- sample1

sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)

sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS1))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample2$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

resCLCNUTS1full = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)


plotResu(resCLCNUTS1full,
         nom = "NUTS1CLCall",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)




## 3.2  500 m and clhs subsample -------------



rnd.lhs500 = clhs::clhs(sample1500[ , X ],
                     size=5000,
                     iter=20000,
                     progress=T,
                     simple=F
                     
)

sample2 <- sample1500[rnd.lhs500$index_samples,]


sample2$id <- c(1:nrow(sample2))






sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)

sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS1))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample2$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


resCLCNUTS1500 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)


plotResu(resCLCNUTS1500,
         nom = "NUTS1CLCall500",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)


save.image("output/first_test.RData")


# 4 Test with LU x NUTS2 -------------

## 4.1 with 100 m and clhs subsample -------------


sample2 <- sample1[rnd.lhs$index_samples,]

# simplified LU
sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)

sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS2))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample2$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))



resCLCNUTS2 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)


plotResu(resCLCNUTS2,
         nom = "NUTS2CLCclhs",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)



## 4.1  100 m and full data -------------

sample2 <- sample1

sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)

sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS2))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample2$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

resCLCNUTS2full = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)


plotResu(resCLCNUTS2full,
         nom = "NUTS2CLCall",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)




save.image("output/first_test.RData")


## 4.2  500 m and clhs subsample-------------




sample2 <- sample1500[rnd.lhs500$index_samples,]
sample2$id <- c(1:nrow(sample2))





sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)

sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS2))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample2$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

resCLCNUTS2500 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)


plotResu(resCLCNUTS2500,
         nom = "NUTS2CLCall500",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500
)


save.image("output/first_test.RData")

# 5 by adding RMQS sites-------
#load french data
disk = "/media/communs_infosol/"
load(paste0(disk,"Projets/RMQS_ETM/Thomas-ETM/Concentration ETM/data_ETM_C_H1.RData"))#Donnees ponctuelles

smn = data_ETM_C_H1%>%
  select(id_site,x_reel,y_reel) %>%
  st_as_sf(coords = 2:3,
           crs = 2154) %>%
  st_transform(crs(stack))

smnsample <- terra::extract(stack,smn)

smnsample$x = data_ETM_C_H1$x_reel
smnsample$y  = data_ETM_C_H1$x_reel

smnsample <- filter(smnsample, !FR_CLC %in% c(1:9, 30,31, 33, 34, 35, 37:43))


smnsample$FR_CLC <- as.factor(smnsample$FR_CLC)
smnsample$FR_NUTS2 <- as.factor(smnsample$FR_NUTS2)
smnsample$FR_NUTS1 <- as.factor(smnsample$FR_NUTS1)

smnsample = na.omit(smnsample)

smnsample$type = "smn"

# start from the 80k points
sample2 <- sample1
sample2$type = "SI"

sample2 = rbind(sample2,
                smnsample %>%
                  select(x,y,FR_OC:type)
                )

sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)


# simplified LU
sample2$FR_CLC_S = occuprecode(sample2$FR_CLC)

sample2$domains <- as.factor(interaction(sample2$FR_CLC_S, sample2$FR_NUTS1))
tab1 <- table(sample2$domains)
sample3 <- sample2[sample2$domains %in% names(tab1[tab1 >= 10]),] 
sample3$domains <- as.factor(as.character(sample3$domains)) 

sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample2$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))

# 
# resCLCNUTS2500 = optimisesampleSML(
#   framesamp = sample3 %>%
#     filter(type=="SI"),
#   framecens= sample3 %>%
#     filter(type !="SI") ,
#   X = X,
#   Y = Y,
#   domainvalue = "domains" ,
#   cv = cv
# )
# 


save.image("output/first_test.RData")

