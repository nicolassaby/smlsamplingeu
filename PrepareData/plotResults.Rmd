---
title: "First results SML sampling exercise"
author: "Nicolas Saby"
output: html_document
date: "2024-02-08"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
load("../output/first_test.RData")

stac500a = rast("~/serena/data/sml/europe/stack500.tiff")

```

# comparison with 3 different sets of starting samples


Here we compare the computed required sample sizes using  NUTS1 only as Domain and with different starting samples:

1) A 5k sample selected by clhs based on 80k SI sample as proposed by JRC.
2) A 80k SI sample
3) A 20k SY sample 

SI = random sampling
SY = systematic sampling

The SY sample gave a larger required sample size. 


There is a  very large differences in computed required sample sizes. 



```{r}
# Results for 1
print(c( "CLHS 5k sample", resNUTS1Sallprop$NbSample))

# Results for 2
print(c( "A 80k SI sample", resNUTS1allpropLargeSample$NbSample) )

# Results for 3
print(c( "A 20k SY sample", resGRid10k$NbSample))
```


Here are the maps

For the 5K starting sample

![Sample5K](../output/NUTS1allclhs.jpeg)
For the 80K starting sample

![test](../output/NUTS1allLarge.jpeg)
For a grid as starting sample

![Sample5K](../output/Nuts1AllGRid10k.jpeg)

```{r, include=FALSE,eval=FALSE}


sample2 <- filter(sample1, !FR_CLC %in% c(1:9, 30,31, 33, 34, 35, 37:43))


sample2$FR_CLC <- as.factor(sample2$FR_CLC)
sample2$FR_NUTS2 <- as.factor(sample2$FR_NUTS2)
sample2$FR_NUTS1 <- as.factor(sample2$FR_NUTS1)

points4 <- sample2[resNUTSallprop$smlsample$ID,]
# coordinates(points4)=~x+y
points4 <- st_as_sf(points4,
                      coords=c("x","y"),
                      crs = crs(stac500a)
)

p1 = tm_shape(stac500a["FR_pH"]) + 
    tm_raster(style = "quantile",n = 10,
              palette = "RdYlGn",
              alpha = 0.5) +
    tm_shape(points4) +
    tm_dots(col = "black",
            size = 0.002)+
    tm_layout(legend.outside = T)

  
 
points4 <- sample2[resNUTSallpropLargeSample$smlsample$ID,]
# coordinates(points4)=~x+y
points4 <- st_as_sf(points4,
                      coords=c("x","y"),
                      crs = crs(stac500a)
)
  
p2 = tm_shape(stac500a["FR_pH"]) + 
    tm_raster(style = "quantile",n = 10,
              palette = "RdYlGn",
              alpha = 0.5) +
    tm_shape(points4) +
    tm_dots(col = "black",
            size = 0.002)+
    tm_layout(legend.outside = T)
tmap_arrange(p1,p2)
```



#  JRC scenarios

We are removing some land uses

```{r, eval=FALSE}
sample1 <- filter(sample1, !FR_CLC %in% c(1:9, 30,31, 33, 34, 35, 37:43))

```

and I reclassified the  land use as 

```{r eval=FALSE}
  recode(attr, 
         "10" = "0" , "11"= "0" , 
         "12"= "1" , "14"= "1" ,
         "15" = "2" ,"16"= "2" , "17"= "2" , 
         "18" = "3" ,"19"="3",  "20" = "3" ,
         "21" = "3" ,"22"  = "3","
                          23" = "4" ,  "24"  = "4" ,"25" = "4" ,
         "26" = "3" , "27"  = "3" ,"28" = "3" ,
         "32" = "3",
         .default = "A")

```



## test  with NUTS 1 and CLC reclassified

With NUTS1 and CORINE LAND COVER reclassified as domain



### scenario A1 100 m


```{r}
resCLCNUTS1clhs$NbSample

```


![test](../output/NUTS1CLCclhs.jpeg)

### scenario A1 500 m

For the 500m grids

```{r}
resCLCNUTS1500$NbSample

```


![test](../output/NUTS1CLCall500.jpeg)



## test  with NUTS 2 and CLC reclassified with French data

The available covariates are not the same

("FR_OC",  "FR_pH","FR_TXT_1",  "FR_TXT_2",  "FR_CEC")

```{r}
load("../output/first_testFrance.RData")
```

### scenario A1 100 m


```{r}

resCLCNUTS2fullFrance$NbSample

```


![test](../output/NUTS2CLCallFrance.jpeg)






## test  with NUTS 2 and CLC reclassified


### scenario A1 100 m


```{r}
resCLCNUTS2$NbSample

```


![figure](../output/NUTS2CLCclhs.jpeg)

### scenario A1 500 m

For the 500m resolution grids

```{r}
resCLCNUTS2500$NbSample

```



![figure 500m resolution](../output/NUTS2CLCall500.jpeg)


