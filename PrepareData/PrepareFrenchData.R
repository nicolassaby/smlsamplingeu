# Copy file locally
file.copy("/media/communs_infosol/Projets/GlobalSoilMap.net/GSM_90m/lehmann/export_tif.zip",
          "~/serena/data/sml/france/")

library(terra)
setwd("~/serena/data/sml/europe//")
stackb <- rast(c("FR_CLC.tif",  "FR_NUTS2.tif","FR_NUTS1.tif")) 

setwd("~/serena/data/sml/france/export_tif/")
stackal93 <- rast(c("soc_moy_30.tif", "ph_moy_30.tif" , "clay_moy_30.tif","silt_moy_30.tif",
                 "cec_moy_30.tif") ) 

stacka <-terra::project(stackal93,crs(stackb), thread = TRUE)

x <- terra::extend( stackb, ext(stacka))
# stackb.1 <- terra::resample(stackb, terra::crop(x, stackb, "out"))
stackb.1 <- terra::resample(stackb, stacka, method= "near" )



stack = c(stacka,stackb)

terra::writeRaster(stack,filename = "~/serena/data/sml/france/stack.tiff",
                   overwrite = TRUE)


  # Change resolution
  stac500a <- terra::aggregate(stacka, 5, fun=mean)
  stac500b <- terra::aggregate(stackb.1, 5, fun=modal)
  stack500 = c(stac500a,stac500b)
  
  terra::writeRaster(stack500,filename = "~/serena/data/sml/france/stack500.tiff",
                     overwrite = TRUE)
  

  
# Adding soil for raster from france
grille = rast("~/serena/data/sml/france/stack.tiff")
library(sf)
soil <- st_read("~/serena/data/sml/europe/eusr5000.shp") %>%
  st_transform(crs(grille))

soilR <- rasterize(soil, grille, field = "SOILNR")


