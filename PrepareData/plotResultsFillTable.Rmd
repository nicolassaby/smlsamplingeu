---
title: "First results SML sampling exercise"
author: "Nicolas Saby"
output: html_document
date: "2024-02-08"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
load("../output/first_test.RData")

stac500a = rast("~/serena/data/sml/europe/stack500.tiff")
sol <- rast("~/serena/data/sml/europe/soilRegion.tiff")

library(RColorBrewer)
```

# JRC data

Here the maps of the input variables

```{r}
plot(stac500a[[X]])
```
The Soil Regions of the European Union and Adjacent Countries 1 : 5 000 000 

```{r}

nb.cols <- 20
mycolors <- colorRampPalette(brewer.pal(8, "Paired"))(nb.cols)

r2 = as.factor(sol)

plot(r2, col = mycolors)

barplot(r2)
```


#  To fill the JRC table

Here is the results of the different scenarios

```{r}
load("../output/FillJRCTableResults.RData")

```

```{r}
cbind.data.frame(
  scenario = c("A1","A2","A4","A5","A6",
               "B1","B2","B3"),
  nSites = c(testA1$NbSample,testA2$NbSample,testA4$NbSample,testA5$NbSample,
             testA6$NbSample,
             testB1$NbSample,testB2$NbSample,testB3$NbSample)
)

```


![Scenario A1 with with Lucas  predictions of pH as illustrative soil property ](../output/testA1.jpeg)

![Scenario A5 with with Lucas  predictions of pH as illustrative soil property](../output/testA5.jpeg)

![Scenario A6 with with Lucas  predictions of  pH as illustrative soil property](../output/testA6.jpeg)

![Scenario B2 with national predictions of pH as illustrative soil property](../output/testB2.jpeg)


# comparison with 3 different sets of starting samples


Here we compare the computed required sample sizes using  NUTS1 only as Domain and with different starting samples:

1) A 5k sample selected by clhs based on 80k SI sample as proposed by JRC.
2) A 80k SI sample
3) A 20k SY sample 

SI = random sampling
SY = systematic sampling

The SY sample gave a larger required sample size. 


There is a  very large differences in computed required sample sizes. 



```{r}
# Results for 1
print(c( "CLHS 5k sample", resNUTS1Sallprop$NbSample))

# Results for 2
print(c( "A 80k SI sample", resNUTS1allpropLargeSample$NbSample) )

# Results for 3
print(c( "A 20k SY sample", resGRid10k$NbSample))
```


Here are the maps

For the 5K starting sample

![Sample5K](../output/NUTS1allclhs.jpeg)
For the 80K starting sample

![test](../output/NUTS1allLarge.jpeg)
For a grid as starting sample

![Sample5K](../output/Nuts1AllGRid10k.jpeg)

# Prise en compte des sols

![Sample5K](../output/testA6.jpeg)
