

# Code pour faire tourner les 4 scénarios, à 2 niveaux (500 et 100m) avec les points RMQS


# chargement des packages
library(sf)
library(terra)
library(ggplot2)
library(dplyr)
library(tidyr) 
library(SamplingStrata) 
library(clhs) 
library(viridis)
library(tmap)


set.seed(123) # not done by JRC but important 

#Replace default kmeans function to use a different algorithm (to avoid convergence issues)
kmeans.default <- function(x, algorithm="MacQueen", ...){base::kmeans.default(x, algorithm, ...)}

# load the specific functions for this project
source("/home/lpotel/smlsamplingeu/functionssML.R")


# 1/ Préparation des données -----

setwd("/home/nsaby/serena/data/sml/europe/")
stacka <- rast(c("FR_OC.tif", "FR_pH.tif" , "FR_TXT.tif","FR_N.tif","FR_P.tif",
                 "FR_CEC.tif","FR_BD010.tif") ) 

sol <- rast("/home/lpotel/smlsamplingeu/export_data/soilRegion.tiff")

stackb <- rast(c("/home/lpotel/smlsamplingeu/data/FR_CLC_ext.tif", 
                 "/home/lpotel/smlsamplingeu/data/FR_metzger_ext.tif",
                 "/home/lpotel/smlsamplingeu/data/FR_regions_ext.tif",
                 "/home/lpotel/smlsamplingeu/data/FR_WRB_ext.tif",
                 "/home/lpotel/smlsamplingeu/data/FR_IGCS_ext.tif",
                 "soilRegion.tiff")) 

stack = c(stacka,stackb)

stack500 = rast("/home/lpotel/smlsamplingeu/export_data/stack500.tif")


setwd("~/serena/smlsamplingeu")


# JRC configuration -----------

# initiatilisation of the variables for the algorithm

# the target variables
X= Y = c("FR_OC",  "FR_pH","FR_TXT_1",  "FR_TXT_2",  "FR_P","FR_N","FR_BD010")


# set the numbers of pixels to sample
clhsNb = 15000
SIsample = 100000

# the list of clc codes to exclude
excludeCLC = c(1,2,3,4,5,6,7,8,9,
               30,31,34,35,37,38,39,40,
               41,42,43,44,48,49,50
)

# ## prepare 100 m sample  ---------
# 
# # Random sampling of pixels of the stack
# sample1 <- spatSample(stack,
#                       SIsample,
#                       na.rm=TRUE ,
#                       "random", 
#                       values=TRUE, 
#                       xy=TRUE)
# 
# # filtering clc
# sample1 <- filter(sample1, !FR_CLC_ext %in% excludeCLC )
# 
# # define factor for the following columns
# sample1$FR_CLC_ext <- as.factor(sample1$FR_CLC_ext)
# sample1$FR_regions_ext <- as.factor(sample1$FR_regions_ext)
# sample1$WRBLEV1<- as.factor(sample1$WRBLEV1)
# sample1$FR_IGCS_ext <- as.factor(sample1$FR_IGCS_ext)
# sample1$FR_metzger_ext <- as.factor(sample1$FR_metzger_ext)
# sample1$SOILNR <- as.factor(sample1$SOILNR)
# 
# 
# saveRDS(sample1,"~/smlsamplingeu/data/sample1.rds")
sample1 <- readRDS("/home/lpotel/smlsamplingeu/data/sample1.rds")
## prepare 500 m sample  ---------

# Random sampling of pixels of the stack
# sample1500 <- spatSample(stack500,
#                          SIsample,
#                          na.rm=TRUE ,
#                          "random", 
#                          values=TRUE, 
#                          xy=TRUE)
# 
# # filtering CLC codes
# sample1500 <- filter(sample1500, !FR_CLC_ext %in% excludeCLC )
# 
# # define factor for the following columns
# sample1500$FR_CLC_ext <- as.factor(sample1500$FR_CLC_ext)
# sample1500$FR_regions_ext <- as.factor(sample1500$FR_regions_ext)
# 
# saveRDS(sample1500,"/home/lpotel/smlsamplingeu/data/sample1500.rds")
sample1500 <- readRDS("/home/lpotel/smlsamplingeu/data/sample1500.rds")
# sample1500$FR_NUTS1 <- as.factor(sample1500$FR_NUTS1)

## run CLHS -----

# running the clhs algorithm to get the id of the pixels to keep

# for 100m grid
# rnd.lhs = clhs::clhs(sample1[ , X ],
#                      size=clhsNb,
#                      iter=10000,
#                      progress=T,
#                      simple=F
# )
# 
# # for 500m grid
# rnd.lhs500 = clhs::clhs(sample1500[ , X ],
#                         size=clhsNb,
#                         iter=10000,
#                         progress=T,
#                         simple=F
# )
# 
# saveRDS(rnd.lhs,"~/smlsamplingeu/data/rnd.lhs.rds")
# saveRDS(rnd.lhs500,"~/smlsamplingeu/data/rnd.lhs500.rds")

rnd.lhs<-readRDS( "/home/lpotel/smlsamplingeu/data/rnd.lhs.rds")
rnd.lhs500<-readRDS( "/home/lpotel/smlsamplingeu/data/rnd.lhs500.rds")


###########################################################################################
# prepare RMQS data --------
# 

# load("~/smlsamplingeu/data/data_ETM_C_H1.RData")#Donnees ponctuelles
# 
# smn = data_ETM_C_H1%>%
#   select(id_site,x_reel,y_reel) %>%
#   st_as_sf(coords = 2:3,
#            crs = 2154) %>%
#   st_transform(crs(stack500))
# 
# smnsample <- terra::extract(stack,
#                             smn)
# # 
# # 
# smnsample$x = st_coordinates(smn)[,1]
# smnsample$y  = st_coordinates(smn)[,2]
# 
# 
# smnsample$FR_CLC_ext <- as.factor(smnsample$FR_CLC_ext)
# smnsample$FR_regions_ext <- as.factor(smnsample$FR_regions_ext)
# # smnsample$FR_NUTS1 <- as.factor(smnsample$FR_NUTS1)
# smnsample = na.omit(smnsample)
# 
# # create a column to identify points from the soil monitoring network
# smnsample$type = "smn"
# 
# 
#  saveRDS(smnsample, "~/smlsamplingeu/data/smnsample.rds")
smnsample <-  readRDS("/home/lpotel/smlsamplingeu/data/smnsample.rds")



#######################################################################################################

# Réalisation des Tests Avec tous les points RMQS -----

# ICI pour S1 à 100m

sample2 = as.data.frame(sample1)

sample3 <-  smnsample

# Include points from existing surveys (forcing to keep existing points)
# Suppose we have a regular grid we want to preserve in total


# sample1 <- sampleRandom(stack1, 80000, xy=T)
# sample2 <- as.data.frame(sample1)
# 
# #Remove areas where soil is not present
# 
# sample2 <- filter(sample2, !FR_CLC %in% c(1:9, 30, 33, 34, 35, 37:43))


#--------------------------------------------------------------------------------------------------
#Convert CLC classes from numerical to factors. Round numerical values to speed up clutering computations
#--------------------------------------------------------------------------------------------------
# sample2$FR_CLC <- as.factor(sample2$FR_CLC)
# sample2$FR_NUTS2 <- as.factor(sample2$FR_NUTS2)
# sample2$FR_OC <- round(sample2$FR_OC)
# sample2$FR_N <- round(sample2$FR_N, 1)
# sample2$FR_BD010 <- round(sample2$FR_BD010, 1)
# sample2$FR_pH <- round(sample2$FR_pH, 1)

# sample2.sp <- sample2
# coordinates(sample2.sp)=~x+y
# 
# #--------------------------------------------------------------------------------------------------
# #Reduce initial sample and optimize soil properties sampling space by latin hypercube sampling
# #--------------------------------------------------------------------------------------------------
# #rnd.lhs = clhs::clhs(sample2[,c(4,6:9)], size=5000, iter=20000, progress=T, simple=F, use.coords=T)
# rnd.lhs = clhs::clhs(sample2.sp[,c(1:10)], size=5000, iter=2000, progress=T, simple=F, use.coords=T)

sampleRand <- sample1[rnd.lhs$index_samples,]

sampleRand$FR_regions_ext<- as.factor(sampleRand$FR_regions_ext)

# simplified LU using personal function if necessary !!
sample2$FR_CLC_S =  occuprecode(sample2$FR_CLC_ext)

tab1500 <- table(sampleRand$FR_regions_ext)
sampleRand <- sampleRand[sampleRand$FR_regions_ext %in% names(tab1500[tab1500 >= 10]),] 

#--------------------------------------------------------------------------------------------------
# Add legacy points to the LHS sample
#--------------------------------------------------------------------------------------------------
tab1500 <- table(sample3$FR_regions_ext)
sample7 <- sample3[sample3$FR_regions_ext %in% names(tab1500[tab1500 >= 10]),] 


sample8 <- rbind(sample7  %>%
                   dplyr::select(FR_OC:FR_regions_ext,x,y) ,
                 sampleRand %>%
                   dplyr::select(FR_OC:FR_regions_ext,x,y)
)


#tab1 <- table(sample8$FR_NUTS2)
#sample8 <- sample8[sample8$FR_NUTS2 %in% names(tab1[tab1 >= 10]),] 
sample8$id <- c(1:nrow(sample8))

ndom <- length(unique(sample8$FR_regions_ext))

frame8 <- buildFrameDF(df = sample8,
                       id = "id",
                       X = X,
                       Y = Y,
                       domainvalue = "FR_regions_ext")

strata8 <- buildStrataDF(frame8, progress=T)

ndom <- length(unique(sample8$FR_regions_ext))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         
                         domainvalue=c(1:ndom)))

#--------------------------------------------------------------------------------------------------
#Find initial solutions for each domain (NUTS2) by kmeans clustering
#--------------------------------------------------------------------------------------------------
init_sol8 <- KmeansSolution2(frame=frame8,
                             errors=cv,
                             maxclusters = 8) 

nstrata8 <- tapply(init_sol8$suggestions,
                   init_sol8$domainvalue,
                   FUN=function(x) length(unique(x)))

initial_solution8 <- prepareSuggestion(init_sol8,frame8,nstrata8)

#--------------------------------------------------------------------------------------------------
#Optimize initial solution by Genetic Algorithm
#--------------------------------------------------------------------------------------------------
ind_framecens <- c(rep(T, dim(sample7)[1]), rep(F, dim(sampleRand)[1]))
framecens <- frame8[ind_framecens,]
#----Selection of units to be sampled from the frame
# (complement to the previous)
framesamp <- frame8[ind_framecens!=T,]

solution8 <- optimStrata(method = "continuous",
                         errors = cv, 
                         framesamp = framesamp,
                         framecens = framecens,
                         iter = 50,
                         pops = 20,
                         nStrata = nstrata8,
                         suggestions = initial_solution8)

framenew8 <- solution8$framenew
outstrata8 <- solution8$aggr_strata

strataStructure8 <- summaryStrata(framenew8,
                                  outstrata8,
                                  progress=T)

#--------------------------------------------------------------
#Total number of samples required
#--------------------------------------------------------------
sum(strataStructure8$Allocation)


sample9 <- selectSample(framenew8, 
                        outstrata8,
                        writeFiles = F)

points8 <- sample8[sample9$ID,]

plot(y~x, sample7)
points(points8$x, points8$y, pch=3, col="red")

