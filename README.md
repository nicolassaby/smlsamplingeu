---
editor_options: 
  markdown: 
    wrap: 72
---

# Introduction

this project to share the statistical works (EJP and DIRSOL projects) on
sampling exercise for Soil monitoring law

# codes

## projet DIRSOL

the folder gather the codes for the project dirsol

the main scripts are the ones names SIMUL_DIRSOL\_\*

these codes use the 2 following codes

-   0_initialisation\*.R :

-    ComputeSampleOptim

## Tests with EJP team

## summary

use 2-SMLFilltableJRCTests.R to run different tests required. use
1-DataPreparationJRC.R to prepare the rasters from JRC sharepoint

you need to create the folders output/ to save the plots

A webpage allow to acces to some results

<https://smlsamplingeu-nicolassaby.pages.mia.inra.fr/>

## Questions

1.  among the input data received for Italy, we have IT_TXT, that is, a
    categorical dataset on textural classes. Instead we did not receive
    the IT_CLAY, IT_SILT, IT_SAND, is it the same for France and
    Netherlands? XX_TXT is a multiband raster, you will get 3 rasters
    once imported in R

2.  Problems and doubts with the scripts:

LINE 19 - "stack1" texture is not included but Copper is included; this
seams in contradiction with what we understood during the meetings held
about the 5 testing. The soil parameters to be included were: OC, N, P,
pH, BD, SILT, SAND, CLAY.

-   yes, we need to set up a clear framework

LINE 19 - both CLC and NUTS2 are considered in the stack and we don't
understand why domains and soil properties are stacked together

-   CLC and NUTS are used as domain,not as strata in the algorithm

LINE 25 & LINE 51 -- in the common script is reported 200000 and 30000
respectively for the number of random samples to allocate and for the
clhs number. We understood that these number would be given by JRC to
each member state, therefore we think we have to ask for those numbers
to Ballabio, right?

= ?

LINE 32 -- a filter on "non soil area" is applied to CLC. In order to
reproduce this filter on the national land use maps, the CLC-codes used
are needed. Do the Belgian colleagues already know those codes?

-   it is available online
    <https://clc.gios.gov.pl/doc/clc/CLC_Legend_EN.pdf>

LINE 60 -- We understood from the Directive that it was obligatory to
apply the Bethel algorithm. Instead, it seems from the script which it
is optional. If it is optional, in which cases is it to be applied?

-   the bethel algorithm is implemented in the function optimStrata as
    each step of the optimiasation

LINE 85 to 91- In the textual description it is said "#Build initial
dataframe of target properties (reporting at NUTS2 by CLC class level)",
but in the script we find \< domainvalue = "NUTS_ID"\>. How the CLC
class level is included? It seems contradictory. #Build initial
dataframe of target properties (reporting at NUTS2 by CLC class level)
frame1 \<- buildFrameDF(df = sample2, id = "id", X = c("BE_OC", "BE_N",
"BE_P", "BE_pH"), Y = c("BE_OC","BE_N", "BE_P", "BE_pH"), domainvalue =
"NUTS_ID")

-   yes it is not consitent. we have to set a clear framework

LINE 108- the given number of maxcluster is 8, do we need to change it?

-   it is not clear, this number is used just for the starting point per
    domain. do not read it as a global number

------------------------------------------------------------------------
