#  CODE 1 / PREPARATION DES RASTER ET DES SHAPEFILES POUR LES STACKS
#  la version ci dessous correspond  la création du stack avec LULUCF

library(sf)
library(terra)
library(ggplot2)
library(dplyr)
library(tidyr) 
library(clhs) 
library(viridis)
library(tmap)

# 1 Data preparation -----


stacka <- rast(c("~/smlsamplingeu/data/stacka/FR_OC.tif", 
                 "~/smlsamplingeu/data/stacka/FR_pH.tif" ,
                 "~/smlsamplingeu/data/stacka/FR_TXT.tif",
                 "~/smlsamplingeu/data/stacka/FR_N.tif",
                 "~/smlsamplingeu/data/stacka/FR_P.tif",
                 "~/smlsamplingeu/data/stacka/FR_CEC.tif",
                 "~/smlsamplingeu/data/stacka/FR_BD010.tif") ) 




# 2 prepare soil region data --------

## soil region
soil <- st_read("~/smlsamplingeu/data/eusr5000.shp") %>%
  st_transform(crs(stacka))

soilR <- rasterize(soil, stacka, field = "SOILNR")
tst = as.data.frame(soilR)


stacka <- rast(c("~/smlsamplingeu/data/FR_OC.tif") ) 

soilRextr <- ifel(is.na(stacka), NA, soilR)

freq(soilRextr)

stack = c(stacka,soilRextr)

plot(soilRextr)


terra::writeRaster(soilRextr,filename = "~/smlsamplingeu/export_data/soilRegion.tif",
                   overwrite = TRUE)

#ALIGNEMENT DES RASTERS A 2 RESOLUTIONS----

## WRB 1----

### 100M
wrb1100 <- rast("~/smlsamplingeu/data/shp_pour_raster/soil_map_WRB_final.tif")

wrb2100 <- terra::project(wrb1100,
                       stacka,
                       method= "near")

# wbr3 <- crop(wrb2 , stacka[[1]] ,mask=TRUE)
wrb3100 <- mask(wrb2100 , stacka[[1]])

# test = c(stacka,wrb3100)
plot(wrb3100)

terra::writeRaster(wrb3100,filename = "~/smlsamplingeu/export_data/FR_WRB_100m.tif",
                   overwrite = TRUE)

rm(wrb1100,wrb2100)

### 500M

FR_WRB1500 <- rast("~/smlsamplingeu/data/shp_pour_raster/soil_map_WRB_final.tif")

wrb2500 <- terra::project(FR_WRB1500,
                          stac500a,
                          method= "near")

# wbr3 <- crop(wrb2 , stacka[[1]] ,mask=TRUE)
wrb3500 <- mask(wrb2500 , stac500a[[1]])

test = c(stac500a,wrb3500)
plot(wrb3500)

terra::writeRaster(wrb3500,filename = "~/smlsamplingeu/export_data/FR_WRB_500m.tif",
                   overwrite = TRUE)

rm(wrb1500,wrb2500)


##Metzger ------

##100m

FR_Metzg1100 <- rast("~/smlsamplingeu/data/shp_pour_raster/climate_metzeger.tif")

FR_Metzg2100 <- terra::project(FR_Metzg1100,
                          stacka,
                          method= "near")

 
FR_Metzg3100 <- mask(FR_Metzg2100 , stacka[[1]])

test = c(stacka,FR_Metzg3100)
plot(FR_Metzg3100)

terra::writeRaster(FR_Metzg3100,filename = "~/smlsamplingeu/export_data/FR_metzger_100m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1100,FR_Metzg2100)




##500m

FR_Metzg1500 <- rast("~/smlsamplingeu/data/shp_pour_raster/climate_metzeger.tif")

FR_Metzg2500 <- terra::project(FR_Metzg1500,
                               stac500a,
                               method= "near")


FR_Metzg3500 <- mask(FR_Metzg2500 , stac500a[[1]])

test = c(stac500a,FR_Metzg3500)
plot(FR_Metzg3500)
terra::writeRaster(FR_Metzg3500,filename = "~/smlsamplingeu/export_data/FR_metzger_500m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1500,FR_Metzg2500)



## CLC------

##100m

FR_CLC <- rast("~/smlsamplingeu/data/stackb/FR_CLC.tif")

FR_CLC2100 <- terra::project(FR_CLC,
                               stacka,
                               method= "near")


FR_CLC3100 <- mask(FR_CLC2100 , stacka[[1]])

test = c(stacka,FR_CLC3100)
freq(FR_CLC3100)

plot(FR_CLC3100)

terra::writeRaster(FR_CLC3100,filename = "~/smlsamplingeu/data/stackb/FR_CLC_100m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1100,FR_Metzg2100)

##500m

FR_CLC500 <- rast("~/smlsamplingeu/data/FR_CLC.tif")

FR_CLC2500 <- terra::project(FR_CLC500,
                             stac500a,
                             method= "near")


FR_CLC3500 <- mask(FR_CLC2500 , stac500a[[1]])

test = c(stac500a,FR_CLC3500)
freq(FR_CLC3500)

plot(FR_CLC3500)

terra::writeRaster(FR_CLC3500,filename = "~/smlsamplingeu/export_data/FR_CLC_500m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1100,FR_Metzg2100)

# LULUCF-----

FR_LULUCF <- rast("~/smlsamplingeu/export_data/FR_landuse.tif")

FR_CLC2100 <- terra::project(FR_LULUCF,
                             stacka,
                             method= "near")


FR_CLC3100 <- mask(FR_CLC2100 , stacka[[1]])

test = c(stacka,FR_CLC3100)
freq(FR_CLC3100)

plot(FR_CLC3100)

terra::writeRaster(FR_CLC3100,filename = "~/smlsamplingeu/export_data/FR_LULUCF_100m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1100,FR_Metzg2100)

##500m

FR_CLC500 <- rast("~/smlsamplingeu/export_data/FR_landuse.tif")

FR_CLC2500 <- terra::project(FR_CLC500,
                             stac500a,
                             method= "near")


FR_CLC3500 <- mask(FR_CLC2500 , stac500a[[1]])

test = c(stac500a,FR_CLC3500)
freq(FR_CLC3500)

plot(FR_CLC3500)

terra::writeRaster(FR_CLC3500,filename = "~/smlsamplingeu/export_data/FR_LULUCF_500m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1100,FR_Metzg2100)



# Shapefile----

## GER IGCS
### 100 m
soil <- st_read("~/smlsamplingeu/data/shp_pour_raster/carte_sol_France.shp") %>%
  st_transform(crs(stacka))

soilR <- rasterize(soil, stacka, field = "ger")

test = c(stacka,soilR)

plot(soilR)

freq(soilR)
terra::writeRaster(soilR,filename = "~/smlsamplingeu/export_data/FR_IGCS_100m.tif",
                   overwrite = TRUE)

 
### 500 m

soil500 <- st_read("~/smlsamplingeu/data/shp_pour_raster/carte_sol_France.shp") %>%
  st_transform(crs(stac500a))

soilR500 <- rasterize(soil500, stac500a, field = "ger")

test = c(stac500a,soilR500)

plot(soilR500)

freq(soilR500)
terra::writeRaster(soilR500,filename = "~/smlsamplingeu/export_data/FR_IGCS_500m.tif",
                   overwrite = TRUE)


## nouvelles_regions-----

### 100m

FR_regions <- st_read("~/smlsamplingeu/data/shp_pour_raster/nouvelles_regions.shp") %>%
  st_transform(crs(stacka))

FR_regions_2100 <- rasterize(FR_regions, stacka, field = "code_supra")

test = c(stacka,FR_regions_2100)

plot(FR_regions_2100)

freq(FR_regions_2100)
terra::writeRaster(FR_regions_2100,filename = "~/smlsamplingeu/export_data/FR_regions_100m.tif",
                   overwrite = TRUE)

### 500m


FR_regions500 <- st_read("~/smlsamplingeu/data/shp_pour_raster/nouvelles_regions.shp") %>%
  st_transform(crs(stac500a))

FR_regions_2500 <- rasterize(FR_regions500, stac500a, field = "code_supra")

test = c(stac500a,FR_regions_2500)
plot(FR_regions_2500)


freq(FR_regions_2500)
terra::writeRaster(FR_regions_2500,filename = "~/smlsamplingeu/export_data/FR_regions_500m.tif",
                   overwrite = TRUE)

# LULUCF-----

FR_LULUCF <- rast("~/smlsamplingeu/export_data/FR_landuse.tif")

FR_CLC2100 <- terra::project(FR_LULUCF,
                             stacka,
                             method= "near")


FR_CLC3100 <- mask(FR_CLC2100 , stacka[[1]])

test = c(stacka,FR_CLC3100)
freq(FR_CLC3100)

plot(FR_CLC3100)

terra::writeRaster(FR_CLC3100,filename = "~/smlsamplingeu/export_data/FR_LULUCF_100m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1100,FR_Metzg2100)

##500m

FR_CLC500 <- rast("~/smlsamplingeu/export_data/FR_landuse.tif")

FR_CLC2500 <- terra::project(FR_CLC500,
                             stac500a,
                             method= "near")


FR_CLC3500 <- mask(FR_CLC2500 , stac500a[[1]])

test = c(stac500a,FR_CLC3500)
freq(FR_CLC3500)

plot(FR_CLC3500)

terra::writeRaster(FR_CLC3500,filename = "~/smlsamplingeu/export_data/FR_LULUCF_500m.tif",
                   overwrite = TRUE)

rm(FR_Metzg1100,FR_Metzg2100)


# CREATION DU STACKB ET DU STACK FINAL : CREATION DU STACK 500M----


stackb <- rast(c("~/smlsamplingeu/export_data/FR_LULUCF_100m.tif", # cette ligne est  remplacer par CLC pour avoir le stack avec les données de Corinne
                 "~/smlsamplingeu/export_data/FR_metzger_100m.tif",
                 "~/smlsamplingeu/export_data/FR_regions_100m.tif",
                 "~/smlsamplingeu/export_data/FR_WRB_100m.tif", 
                 "~/smlsamplingeu/export_data/FR_IGCS_100m.tif",
                 "~/smlsamplingeu/export_data/soilRegion.tif")) 

stack = c(stacka,stackb)

change = TRUE # set true to resample rasters

# if(change) {
#   # Change resolution
stac500a <- terra::aggregate(stacka, 5, fun=mean)
stac500b <- terra::aggregate(stackb, 5, fun=modal)
stack500 = c(stac500a,stac500b)

terra::writeRaster(stack500,filename = "~/smlsamplingeu/export_data/stack500_lulucf.tif",
                   overwrite = TRUE)

# } else {
#   stack500 = rast("~/serena/data/sml/europe/stack500.tiff")
#   
# }



