
# Ce script permet de faire tourner les 4 scénarios aux deux résolutions attendues, en prenant en compte
#  les points du RMQS prospéctés en 2024-2025 et en utilisant les données d'occupation du sol CLC


# initialisations --------
source("dirsol/0_InitialisationsCLC.R")


# Chargement des données rmqs 2024/2025----

# rmqs2024 <- readRDS("/home/lpotel/smlsamplingeu_louis/data/site_rmqs2024.rds")#Données ponctuelles
rmqs2024 <- read.csv2("/home/lpotel/smlsamplingeu/data/data_rmqs/rmqs2024.csv")#Données ponctuelles
load("/home/lpotel/smlsamplingeu/data/data_rmqs/data_ETM_C_H1.RData")

smn = data_ETM_C_H1 %>%
  left_join( rmqs2024, by="id_site")  %>%
  filter(!is.na(x_reel) & !is.na(annee) ) %>% 
  dplyr::select(id_site,x_reel,y_reel) %>%
  st_as_sf(coords = 2:3,
           crs = 2154) %>%
  st_transform(crs(stack))


# tm_shape(stack[["FR_OC"]]) + tm_raster() + tm_shape(smn) + tm_dots()

sample3 <-  terra::extract(stack,
                           smn,
                           xy = TRUE
)

sample3 <- sample3[complete.cases(sample3),]

sample3 <- sample3 %>%
  rename(FR_metzger = climate_metzeger, 
         FR_regions = code_supra, FR_IGCS = ger, 
         FR_WRB = WRBLEV1)




#remove the non soil
sample3 <- filter(sample3, !FR_WRB %in% excludeWRB )


# saveRDS( sample3, "~/smlsamplingeu_louis/data/octobre_avec_rmqs2024_2025_sample3_CLC.rds")
# sample3 <- readRDS("~/smlsamplingeu_louis/data/octobre_avec_rmqs2024_2025_sample3_CLC.rds")


# en forçant la prise en compte de tous les points RMQS 
# Include points from existing surveys (forcing to keep existing points)
# Suppose we have a regular grid we want to preserve in total


echant = "RMQS2024-25"

##  (S1_CLC_JRC) ------
scenario = "S1_CLC_JRC"
resolution = "100m"

ListVarScenario <- c("FR_CLC", 
                     "FR_regions", 
                     "FR_WRB",
                     "FR_metzger")

## 100m-------


domains <- 1e6 * stackb[["FR_CLC"]] +
  1e4 * stackb[["code_supra"]]  +
  1e2 *stackb[["WRBLEV1"]] +
  stackb[["climate_metzeger"]]


sampleRand <- sample1[rnd.lhs$index_samples,]

source("dirsol/ComputeSampleOptim.R")




## 500m ----

resolution = "500m"

domains <- 1e6 * stack500[["FR_CLC"]] + 
  1e4 * stack500[["code_supra"]]  +
  1e2 *stack500[["WRBLEV1"]] + 
  stack500[["climate_metzeger"]]

sampleRand <- sample1500[rnd.lhs500$index_samples,]


source("dirsol/ComputeSampleOptim.R")


# (S2_CLC_JRC) ---------
scenario = "S2_CLC_JRC"
ListVarScenario <- c("FR_CLC", 
                     "FR_regions", 
                     "FR_WRB")

## 100m------
resolution = "100m"

domains <- 1e4 * stackb[["FR_CLC"]] +
  1e2 * stackb[["code_supra"]]  +
  stackb[["WRBLEV1"]]

sampleRand <- sample1[rnd.lhs$index_samples,]



source("dirsol/ComputeSampleOptim.R")




##500m----
resolution = "500m"

domains <- 1e4 * stack500[["FR_CLC"]] + 
  1e2 * stack500[["code_supra"]]  +
  stack500[["WRBLEV1"]] 

sampleRand <- sample1500[rnd.lhs500$index_samples,]


source("dirsol/ComputeSampleOptim.R")



# (S1_CLC_IGCS) -----

scenario = "S1_CLC_IGCS"

ListVarScenario <- c("FR_CLC", "FR_regions", "FR_IGCS", "FR_metzger")

### 100m-------
resolution = "100m"

domains <- 1e6 * stackb[["FR_CLC"]] + 1e4 * stackb[["code_supra"]]  +
  1e2 *stackb[["ger"]] + stackb[["climate_metzeger"]]

sampleRand <- sample1[rnd.lhs$index_samples,]

source("dirsol/ComputeSampleOptim.R")


## 500m-----
resolution = "500m"

domains <- 1e6 * stack500[["FR_CLC"]] + 1e4 * stack500[["code_supra"]]  +
  1e2 *stack500[["ger"]] + stack500[["climate_metzeger"]]

sampleRand <- sample1500[rnd.lhs500$index_samples,]

source("dirsol/ComputeSampleOptim.R")




# (S2_CLC_IGCS) ----------
scenario = "S2_CLC_IGCS"

ListVarScenario <- c("FR_CLC", "FR_regions", "FR_IGCS")

## 100m-------
resolution = "100m"

domains <- 1e4 * stackb[["FR_CLC"]] +
  1e2 * stackb[["code_supra"]]  +
  stackb[["ger"]]





sampleRand <- sample1[rnd.lhs$index_samples,]

source("dirsol/ComputeSampleOptim.R")



## 500m------------
resolution = "500m"

domains <- 1e4 * stack500[["FR_CLC"]] + 
  1e2 * stack500[["code_supra"]]  +
  stack500[["ger"]] 

sampleRand <- sample1500[rnd.lhs500$index_samples,]

source("dirsol/ComputeSampleOptim.R")


