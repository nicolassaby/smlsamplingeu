
#  Script pour faire tourner les simulations SANS RMQS, avec les données CLC----

# initialisations --------
source("dirsol/0_InitialisationsCLC.R")



#  Mise en oeuvre des scenarios ----

 echant = "sansRmqs"


##  (S1_CLC_JRC) 500m ----------

scenario = "S1_CLC_JRC"
resolution = "500m"

# calcul d'de la grille representant les domaines 

domainsS1500 <- 1e6 * stack500[["FR_CLC"]] + 
  1e4 * stack500[["code_supra"]]  +
  1e2 *stack500[["WRBLEV1"]] +
  stack500[["climate_metzeger"]]

# A JRC scenario with default layers (A) 
# selection of the pixels using clhs
sample2 <- sample1500[rnd.lhs500$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", "FR_regions", "FR_WRB", "FR_metzger"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e6 * FR_CLC + 1e4 * FR_regions +
           1e2 * FR_WRB + FR_metzger
  )

# simplified LU using personal function if necessary !!
# sample2$FR_CLC_S = sample2$FR_CLC # occuprecode(sample2$FR_CLC)



tab1 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()

area = sum(tab1$n) # areaDomains10 de domains

areaDomains10 = sum(tab1$n[tab1$n<10]) # areaDomains10 de domains avec plus de 10 pixels

100*(areaDomains10-area) /area

nb_domains1500 <-sum(tab1$n > 10 )

sample3<- sample2 %>% 
  
  left_join(tab1 %>% 
              dplyr::select(domains, n), by= "domains") %>% 
  filter(n>10)


sample3$domains <- as.factor(as.character(sample3$domains)) 
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


# run the optimisation


testA1 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA1, 
#         "/home/lpotel/smlsamplingeu_louis/export_data/S1_CLC_JRC_500m.rds" )

mask_values <- terra::ifel( domainsS1500 %in% tab1$domains[tab1$n>10], 1, 0 )

plot(mask_values)

nb_domainskept <- sum(tab1$n > 10 )
nb_domainsTOT <- sum(tab1$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)

plotResu(testA1,
         nom = "dirsol/figures/S1_CLC_JRC500m",
         crs = crs(stack500),
         sample2 = sample2,
         stack500 = stack500,
         sample3 = sample3,
         mask_values = mask_values
)

source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA1,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S1_CLC_JRC500m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )



## (S1_CLC_JRC) 100m ----------

scenario = "S1_CLC_JRC"
resolution = "100m"

# A JRC scenario with default layers (A) 

# calcul d'de la grille representant les domaines 
domainsS1 <- 1e6 * stackb[["FR_CLC"]] + 
  1e4 * stackb[["code_supra"]]  +
  1e2 *stackb[["WRBLEV1"]] +
  stackb[["climate_metzeger"]]

sample2 <- sample1[rnd.lhs$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", 
              "FR_regions", 
              "FR_WRB", 
              "FR_metzger"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e6 * FR_CLC +
           1e4 * FR_regions +
           1e2 * FR_WRB +
           FR_metzger
  )

# simplified LU using personal function if necessary !!
# sample2$FR_CLC_S = sample2$FR_CLC # occuprecode(sample2$FR_CLC)

tab1100 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()


sum(tab1100$n) # somme totale

sum(tab1100$n[tab1100$n<10]) # somme totale

100*(sum(tab1100$n[tab1100$n<10])-sum(tab1100$n)) /(sum(tab1100$n))

nb_domains1100 <-sum(tab1100$n > 10 )


sample3<- sample2 %>% 
  
  left_join(tab1100 %>% 
              dplyr::select(domains, n), by= "domains") %>% 
  filter(n>10)


sample3$domains <- as.factor(as.character(sample3$domains))
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


# run the optimisation
testA1100 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA1100, "/home/lpotel/smlsamplingeu_louis/export_data/S1_CLC_JRC_100m.rds" )

mask_values <- terra::ifel( domainsS1%in% tab1100$domains[tab1100$n>10], 1, 0 )

nb_domainskept <- sum(tab1100$n > 10 )
nb_domainsTOT <- sum(tab1100$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)



source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA1100,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S1_CLC_JRC100m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )


##(S2_CLC_JRC) 500m --------------

scenario = "S2_CLC_JRC"
resolution = "500m"

# calcul d'de la grille representant les domaines 
domainsS2500 <- 1e4 * stack500[["FR_CLC"]] + 
  1e2 * stack500[["code_supra"]]  +
  stack500[["WRBLEV1"]] 

sample2 <- sample1500[rnd.lhs500$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", "FR_regions", "FR_WRB"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e4 * FR_CLC + 1e2 * FR_regions +
           FR_WRB 
  )

tab2500 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()

sum(tab2500$n) # somme totale

sum(tab2500$n[tab2500$n<10]) # somme totale

100*(sum(tab2500$n[tab2500$n<10])-sum(tab2500$n)) /(sum(tab2500$n))

nb_domains2500 <-sum(tab2500$n > 10 )


sample3<- sample2 %>% 
  
  left_join(tab2500 %>% 
              dplyr::select(domains, n),
            by= "domains") %>% 
  filter(n>10)


sample3$domains <- as.factor(as.character(sample3$domains))
sample3$id <- c(1:nrow(sample3))

ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

testA2500 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA2500, "/home/lpotel/smlsamplingeu_louis/export_data/S2_CLC_JRC_500m.rds" )




mask_values <- terra::ifel( domainsS2500 %in% tab2500$domains[tab2500$n>10], 1, 0 )


nb_domainskept <- sum(tab2500$n > 10 )
nb_domainsTOT <- sum(tab2500$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)



source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA2500,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S2_CLC_JRC500m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )

## (S2_CLC_JRC) 100m --------------

scenario = "S2_CLC_JRC"
resolution = "100m"


# calcul d'de la grille representant les domaines 
domainsS2 <- 1e4 * stackb[["FR_CLC"]] +
  1e2 * stackb[["code_supra"]]  +
  stackb[["WRBLEV1"]]

# selection of the pixels using clhs
sample2 <- sample1[rnd.lhs$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", "FR_regions", "FR_WRB"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e4 * FR_CLC + 1e2 * FR_regions +
           FR_WRB 
  )


# simplified LU using personal function if necessary
# sample2$FR_CLC_S = sample2$FR_CLC # occuprecode(sample2$FR_CLC)


tab2100 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()


sum(tab2100$n) # somme totale

sum(tab2100$n[tab2100$n<10]) # somme totale

100*(sum(tab2100$n[tab2100$n<10])-sum(tab2100$n)) /(sum(tab2100$n))

nb_domains2100 <-sum(tab2100$n > 10 )

sample3<- sample2 %>% 
  
  left_join(tab2100 %>% 
              dplyr::select(domains, n), by= "domains") %>% 
  filter(n>10)

sample3$domains <- as.factor(as.character(sample3$domains))
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

testA2100 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA2100, "/home/lpotel/smlsamplingeu_louis/export_data/S2_CLC_JRC_100m.rds" )
# saveRDS(tab2100, "~/smlsamplingeu_louis/export_data/tab_S2_100m.rds" )

mask_values <- terra::ifel( domainsS2 %in% tab2100$domains[tab2100$n>10], 1, 0 )

nb_domainskept <- sum(tab2100$n > 10 )
nb_domainsTOT <- sum(tab2100$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)



source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA2100,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S2_CLC_JRC100m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )

## (S1_CLC_IGCS) 500m --------------

scenario = "S1_CLC_IGCS"
resolution = "500m"


# calcul d'de la grille representant les domaines 
domainsS3500 <- 1e6 * stack500[["FR_CLC"]] +
  1e4 * stack500[["code_supra"]]  +
  1e2 *stack500[["ger"]] +
  stack500[["climate_metzeger"]]

# selection of the pixels using clhs
sample2 <- sample1500[rnd.lhs500$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", "FR_regions", "FR_IGCS", "FR_metzger"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e6 * FR_CLC + 1e4 * FR_regions +
           1e2 * FR_IGCS + FR_metzger
  )



tab3500 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()

sum(tab3500$n) # somme totale

sum(tab3500$n[tab3500$n<10]) # somme totale

100*(sum(tab3500$n[tab3500$n<10])-sum(tab3500$n)) /(sum(tab3500$n))

nbdomain3500 <- sum(tab3500$n > 10 )


sample3<- sample2 %>% 
  
  left_join(tab3500 %>% 
              dplyr::select(domains, n), by= "domains") %>% 
  filter(n>10)

# nb_domains3500 <-sum(tab3500$n > 10 )

sample3$domains <- as.factor(as.character(sample3$domains))
sample3$id <- c(1:nrow(sample3))

ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


testA3500 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA3500, "/home/lpotel/smlsamplingeu_louis/export_data/S1_CLC_IGCS_500m.rds" )



mask_values <- terra::ifel( domainsS3500 %in% tab3500$domains[tab3500$n>10], 1, 0 )


nb_domainskept <- sum(tab3500$n > 10 )
nb_domainsTOT <- sum(tab3500$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)



source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA3500,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S1_CLC_IGCS500m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )

## (S1_CLC_IGCS) 100m --------------

scenario = "S1_CLC_IGCS"
resolution = "100m"

# calcul d'de la grille representant les domaines 
domainsS3 <- 1e6 * stackb[["FR_CLC"]] + 
  1e4 * stackb[["code_supra"]]  +
  1e2 *stackb[["ger"]] + 
  stackb[["climate_metzeger"]]

# selection of the pixels using clhs
sample2 <- sample1[rnd.lhs$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", "FR_regions", "FR_IGCS", "FR_metzger"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e6 * FR_CLC + 1e4 * FR_regions +
           1e2 * FR_IGCS + FR_metzger
  )

# simplified LU using personal function if necessary
# sample2$FR_CLC_S = sample2$FR_CLC_ext # occuprecode(sample2$FR_CLC)

tab3100 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()


sum(tab3100$n) # somme totale

sum(tab3100$n[tab3100$n<10]) # somme totale

100*(sum(tab3100$n[tab3100$n<10])-sum(tab3100$n)) /(sum(tab3100$n))

nb_domains3100 <-sum(tab3100$n > 10 )

sample3<- sample2 %>% 
  
  left_join(tab3100 %>% 
              dplyr::select(domains, n), by= "domains") %>% 
  filter(n>10)

sample3$domains <- as.factor(as.character(sample3$domains))
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

testA3100 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA3100, "/home/lpotel/smlsamplingeu_louis/export_data/S1_CLC_IGCS_100m.rds" )




mask_values <- terra::ifel( domainsS3%in% tab3100$domains[tab3100$n>10], 1, 0 )
plot(mask_values)

nb_domainskept <- sum(tab3100$n > 10 )
nb_domainsTOT <- sum(tab3100$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)



source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA3100,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S1_CLC_IGCS100m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )

## (S2_CLC_IGCS) 500m --------------

scenario = "S2_CLC_IGCS"
resolution = "500m"

# calcul d'de la grille representant les domaines 
domainsS4500 <- 1e4 * stack500[["FR_CLC"]] +
  1e2 * stack500[["code_supra"]]  +
  stack500[["ger"]] 


# selection of the pixels using clhs
sample2 <- sample1500[rnd.lhs500$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", "FR_regions", "FR_IGCS"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e4 * FR_CLC + 1e2 * FR_regions +
           FR_IGCS 
  )


tab4500 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()


sum(tab4500$n) # somme totale

sum(tab4500$n[tab4500$n<10]) # somme totale

100*(sum(tab4500$n[tab4500$n<10])-sum(tab4500$n)) /(sum(tab4500$n))

nb_domains4500 <-sum(tab4500$n > 10 )

sample3<- sample2 %>% 
  
  left_join(tab4500 %>% 
              dplyr::select(domains, n), by= "domains") %>% 
  filter(n>10)

sample3$domains <- as.factor(as.character(sample3$domains))
sample3$id <- c(1:nrow(sample3))


ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

testA4500 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA4500, "/home/lpotel/smlsamplingeu_louis/export_data/S2_CLC_IGCS_500m.rds" )



mask_values <- terra::ifel( domainsS4500 %in% tab4500$domains[tab4500$n>10], 1, 0 )

plot(mask_values)

nb_domainskept <- sum(tab4500$n > 10 )
nb_domainsTOT <- sum(tab4500$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)



source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA4500,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S2_CLC_IGCS500m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )


## (S2_CLC_IGCS) 100m --------------

scenario = "S2_CLC_IGCS"
resolution = "100m"

# calcul d'de la grille representant les domaines 
domainsS4 <- 1e4 * stackb[["FR_CLC"]] + 
  1e2 * stackb[["code_supra"]]  +
  stackb[["ger"]]



# selection of the pixels using clhs
sample2 <- sample1[rnd.lhs$index_samples,]

sample2 <- sample2 %>% 
  rename(FR_WRB= WRBLEV1) %>%
  mutate_at(c("FR_CLC", "FR_regions", "FR_IGCS"),
            ~as.numeric(as.character(.))
  ) %>%
  mutate(domains = 1e4 * FR_CLC + 1e2 * FR_regions +
           FR_IGCS 
  )

# simplified LU using personal function if necessary
# sample2$FR_CLC_S = sample2$FR_CLC # occuprecode(sample2$FR_CLC)


tab4100 <- sample2 %>% 
  group_by(domains) %>% 
  summarise(n=n()) %>% 
  ungroup()


sum(tab4100$n) # somme totale

sum(tab4100$n[tab4100$n<10]) # somme totale

100*(sum(tab4100$n[tab4100$n<10])-sum(tab4100$n)) /(sum(tab4100$n))

nb_domains4100 <-sum(tab4100$n > 10 )

sample3<- sample2 %>% 
  
  left_join(tab4100 %>% 
              dplyr::select(domains, n), by= "domains") %>% 
  filter(n>10)

sample3$domains <- as.factor(as.character(sample3$domains))
sample3$id <- c(1:nrow(sample3))



ndom <- length(unique(sample3$domains))

cv <- as.data.frame(list(DOM=rep("DOM1",ndom),
                         CV1=rep(0.05,ndom),
                         CV2=rep(0.05,ndom),
                         CV3=rep(0.05,ndom),
                         CV4=rep(0.05,ndom),
                         CV5=rep(0.05,ndom),
                         CV6=rep(0.05,ndom),
                         CV7=rep(0.05,ndom),
                         domainvalue=c(1:ndom)))


#Find initial solutions for each domain (NUTS2) by kmeans clustering

testA4100 = optimisesampleSML(
  framesamp = sample3 ,
  framecens= NULL ,
  X = X,
  Y = Y,
  domainvalue = "domains" ,
  cv = cv
)

# saveRDS(testA4100, "/home/lpotel/smlsamplingeu_louis/export_data/S2_CLC_IGCS_100m.rds" )
 



mask_values <- terra::ifel( domainsS4 %in% tab4100$domains[tab4100$n>10], 1, 0 )

plot(mask_values)

nb_domainskept <- sum(tab4100$n > 10 )
nb_domainsTOT <- sum(tab4100$n>0  )

titreCartetot = paste(titreCarte,"\n",scenario ,resolution)



source("dirsol/PlotResu_sansrmqs.R")


r <- plotResu_sansrmqs(
  res = testA4100,                 # Résultat de l'optimisation
  nom = "dirsol/figures/S2_CLC_IGCS100m", # Nom du fichier
  crs = crs(stack500),          # CRS de stack500
  sample2 = sample2,            # Données d'échantillon
  stack500 = stack500,          # Les données raster
  mask_values = mask_values,    # Masque des valeurs
  sample3 = sample3,            # Données filtrées
  titreCartetot = titreCartetot, # Titre du scénario
  nb_domainskept = nb_domainskept, # Nombre de domaines retenus
  nb_domainsTOT = nb_domainsTOT  # Nombre total de domaines
)

tmap_save(r , file = paste0("dirsol/figures/",scenario,resolution,echant,".jpeg") )

# 