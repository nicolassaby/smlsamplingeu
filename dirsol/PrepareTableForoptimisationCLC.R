# Random sampling of pixels of the stack
sample1 <- spatSample(stack,
                       SIsample,
                       na.rm=TRUE ,
                       "random",
                       values=TRUE,
                       xy=TRUE)
 
sample1 <- sample1 %>%
   rename(FR_metzger = climate_metzeger,
          FR_regions = code_supra, 
          FR_IGCS = ger) 
 
# filtering clc
 sample1 <- filter(sample1, 
                   !FR_CLC %in% excludeCLC 
                   )

# filtering non soil in WRB
 
sample1 <- filter(sample1, 
                  !WRBLEV1 %in% excludeWRB
                  )
 
# define factor for the following columns
sample1$FR_landuse <- as.factor(sample1$FR_landuse)
sample1$FR_regions<- as.factor(sample1$FR_regions)
sample1$WRBLEV1<- as.factor(sample1$WRBLEV1)
sample1$FR_IGCS <- as.factor(sample1$FR_IGCS)
sample1$FR_metzger <- as.factor(sample1$FR_metzger)
sample1$SOILNR <- as.factor(sample1$SOILNR)

saveRDS(sample1,"~/smlsamplingeu_louis/data/28octobre_sample1_lulucf_.rds")

## prepare 500 m sample  ---------

# Random sampling of pixels of the stack
sample1500 <- spatSample(stack500,
                          SIsample,
                          na.rm=TRUE ,
                          "random",
                          values=TRUE,
                          xy=TRUE)
 
sample1500 <-  sample1500 %>% 
   rename(FR_metzger = climate_metzeger,
          FR_regions = code_supra,
          FR_IGCS = ger)
 
# filtering CLC codes
 sample1500 <- filter(sample1500, 
                      !FR_CLC %in% excludeCLC )
 
# filtering non soil in WRB
 
sample1500 <- filter(sample1500, 
                     !WRBLEV1 %in% excludeWRB )
 
# define factor for the following columns
sample1500$FR_landuse<- as.factor(sample1500$FR_landuse)

sample1500$FR_regions <- as.factor(sample1500$FR_regions)
 
saveRDS(sample1500,"~/smlsamplingeu_louis/data/octobre_sample1500_lulucf.rds")
sample1500 <- readRDS("~/smlsamplingeu_louis/data/octobre_sample1500_lulucf.rds")
sample1500$FR_NUTS1 <- as.factor(sample1500$FR_NUTS1)

# run CLHS -----

# running the clhs algorithm to get the id of the pixels to keep

# for 100m grid
rnd.lhs = clhs::clhs(sample1[ , X ],
                      size=clhsNb,
                      iter=10000,
                      progress=T,
                      simple=F
)

# for 500m grid
rnd.lhs500 = clhs::clhs(sample1500[ , X ],
                         size=clhsNb,
                         iter=10000,
                         progress=T,
                         simple=F
 )

saveRDS(rnd.lhs,"~/smlsamplingeu_louis/data/octobre_rnd.lhs_lulucf.rds")
saveRDS(rnd.lhs500,"~/smlsamplingeu_louis/data/octobre_rnd.lhs500_lulucf.rds")
