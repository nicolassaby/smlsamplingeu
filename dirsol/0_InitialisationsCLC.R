#  Chargement des packages
library(sf)
library(terra)
library(ggplot2)
library(dplyr)
library(tidyr) 
library(SamplingStrata) 
library(clhs) 
library(viridis)
library(tmap)

set.seed(123) # not done by JRC but important 

#Replace default kmeans function to use a different algorithm (to avoid convergence issues)
kmeans.default <- function(x, 
                           algorithm="MacQueen", ...){
  base::kmeans.default(x, algorithm, ...)
}

# load the specific functions for this project
source("/home/lpotel/smlsamplingeu/dirsol/functionsssML_Louis.R")
# source("dirsol/functionss_test_sans_rmqs.R")


# Préparation des données -----

stacka <- rast(c("/home/lpotel/smlsamplingeu/data/stacka/FR_OC.tif", 
                 "/home/lpotel/smlsamplingeu/data/stacka/FR_pH.tif" ,
                 "/home/lpotel/smlsamplingeu/data/stacka/FR_TXT.tif",
                 "/home/lpotel/smlsamplingeu/data/stacka/FR_N.tif",
                 "/home/lpotel/smlsamplingeu/data/stacka/FR_P.tif",
                 "/home/lpotel/smlsamplingeu/data/stacka/FR_CEC.tif",
                 "/home/lpotel/smlsamplingeu/data/stacka/FR_BD010.tif") ) 


sol <- rast("/home/lpotel/smlsamplingeu/data/stackb/soilRegion.tif")

stackb <- rast(c("/home/lpotel/smlsamplingeu/data/stackb/FR_CLC_100m.tif", 
                 "/home/lpotel/smlsamplingeu/data/stackb/FR_metzger_100m.tif",
                 "/home/lpotel/smlsamplingeu/data/stackb/FR_regions_100m.tif",
                 "/home/lpotel/smlsamplingeu/data/stackb/FR_WRB_100m.tif", 
                 "/home/lpotel/smlsamplingeu/data/stackb/FR_IGCS_100m.tif",
                 "/home/lpotel/smlsamplingeu/data/stackb/soilRegion.tif")) 

stack = c(stacka,stackb)

stack500 = rast("/home/lpotel/smlsamplingeu/data/stack500.tif")

# Preparation des tableau pour l'optimisation ---

## Extraction pixels des stacks et clhs  --------
# assez long à calculer
calcul = FALSE
if (calcul == TRUE) source("dirsol/PrepareTableForoptimisation.R")

# Preparation des tableaux pour l'optimisation ---

## Extraction pixels des stacks et clhs  --------
# assez long à calculer
calcul = FALSE
if (calcul == TRUE) source("dirsol/PrepareTableForoptimisation.R")

## Chargement des données d'entrée précaclulées -----

sample1 <- readRDS("/home/lpotel/smlsamplingeu/data/quick_load/11septembre_sample1.rds")
sample1500 <- readRDS("/home/lpotel/smlsamplingeu/data/quick_load/11septembre_sample1500.rds")

rnd.lhs <- readRDS( "/home/lpotel/smlsamplingeu/data/quick_load/11septembre_rnd.lhs.rds")
rnd.lhs500 <- readRDS( "/home/lpotel/smlsamplingeu/data/quick_load/11septembre_rnd.lhs500.rds")

 


# configuration -----------

# initiatilisation of the variables for the algorithm

# the target variables
X= Y = c("FR_OC",  "FR_pH","FR_TXT_1",  "FR_TXT_2",  "FR_P","FR_N","FR_BD010")


# set the numbers of pixels to sample
clhsNb = 15000
SIsample = 1000000

excludeWRB = 0

# the list of clc codes to exclude
excludeCLC = c(1,2,3,4,5,6,7,8,9,
               30,31,34,35,37,38,39,40,
               41,42,43,44,48,49,50
               )


titreCarte = "Simulation du réseau de mesures de la SML avec CLC"
